from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import cv2

import glob
import os
import datetime
from PIL import Image, ImageDraw
import numpy as np
import platform
import tflite_runtime.interpreter as tflite

EDGETPU_SHARED_LIB = {
    'Linux': 'libedgetpu.so.1',
    'Darwin': 'libedgetpu.1.dylib',
    'Windows': 'edgetpu.dll'
}[platform.system()]
#setting up paths
HOME_PATH = "/home/pi/"
SCRIPTS_DIR = HOME_PATH + "tflite-edge-tpu-inference/"
PICS_DIRECTORY = HOME_PATH + "birds/"
EMPTY_PICS = HOME_PATH + "empty/"
SORTED_PICS = HOME_PATH + "sorted/"
LOCKFILE = "inference.lock"

def load_labels(path):
  with open(path, 'r') as f:
    return {i: line.strip() for i, line in enumerate(f.readlines())}

def make_interpreter(model_file):
    model_file, *device = model_file.split('@')
    return tflite.Interpreter(
        model_path=model_file,
        experimental_delegates=[
            tflite.load_delegate(EDGETPU_SHARED_LIB,
                                 {'device': device[0]} if device else {})
        ])

def set_input_tensor(interpreter, image):
  tensor_index = interpreter.get_input_details()[0]['index']
  input_tensor = interpreter.tensor(tensor_index)()[0]
  input_tensor[:, :] = image


def classify_image(interpreter, image, top_k=1):
  """Returns a sorted array of classification results."""
  set_input_tensor(interpreter, image)
  interpreter.invoke()
  output_details = interpreter.get_output_details()[0]
  output = np.squeeze(interpreter.get_tensor(output_details['index']))

  # If the model is quantized (uint8 data), then dequantize the results
  if output_details['dtype'] == np.uint8:
    scale, zero_point = output_details['quantization']
    output = scale * (output - zero_point)

  ordered = np.argpartition(-output, top_k)
  return [(i, output[i]) for i in ordered[:top_k]]


def main():
    # check for lock file & create it
    if (not os.path.exists(SCRIPTS_DIR+LOCKFILE)):
        f = open(SCRIPTS_DIR+LOCKFILE, 'w')
    else:
        print("Process already running or has crashed previously!")
        exit(1)
    print("App started")
    pics4deletion = []
    pics2move = []
    labels = load_labels(SCRIPTS_DIR+"birds_labels.txt")

    interpreter = make_interpreter(SCRIPTS_DIR+"mobilenet_v2_1.0_224_quant_edgetpu.tflite")
    interpreter.allocate_tensors()
    _, height, width, _ = interpreter.get_input_details()[0]['shape']
    print("Models loaded")
    counter = 0
    os.chdir(HOME_PATH)
    if (not os.path.exists(SORTED_PICS)):
       # os.mkdir("sorted")
       # os.mkdir("sorted/birds")
        os.mkdir(SORTED_PICS)
    print("Directories created")
    tstart = datetime.datetime.now()
    tstart_global = tstart
    len=glob.glob(PICS_DIRECTORY+'*.jpg').__len__()
    print("Pics to sort: "+ str(len))
    print("Starting sorting")

    # investigate this
    # pic_array
    # worklist = glob.glob(PICS_DIRECTORY+'*.jpg')
    # batchsize = 500
    #
    # for i in xrange(0, len(worklist), batchsize):
    #     batch = worklist[i:i+batchsize] # the result might be shorter than batchsize at the end
    #     # do stuff with batch
    #     for file in batch:
    #         curr_img = Image.open(filename).convert('RGB')
    #         curr_imgcv2 = cv2.cvtColor(np.array(curr_img), cv2.COLOR_RGB2BGR)
    #         curr_imgcv2 = cv2.resize(curr_imgcv2,(224,224))
    #         pic_array.append(curr_imgcv2)
    #     for img in pic_array:
    #         results = classify_image(interpreter,img)
    #         #do the stuff
    os.chdir(PICS_DIRECTORY)
    for filename in glob.glob('*.jpg'):
        if (counter % 500 == 0 and counter > 0):
            tend = datetime.datetime.now()
            time_diff = (tend - tstart)
            tdelta = time_diff.total_seconds()
            print("Number of pictures processed: " + str(counter))
            print("Progress: " + str(round(counter*100/len,2)) + "%")
            # print("Processing time per 500 pics: " + str(tdelta))
            print("Rate: " + str(round(500/tdelta,3))+"pics/s")
            print("Moving pics")
            for file in pics2move:
              os.rename(PICS_DIRECTORY+file, SORTED_PICS+file)
            # delete pics
            print("Deleting pics")
            for file in pics4deletion:
              os.remove(PICS_DIRECTORY+file)
            pics4deletion = []
            pics2move = []
            tstart = datetime.datetime.now()

        curr_img = Image.open(filename).convert('RGB')
        curr_imgcv2 = cv2.cvtColor(np.array(curr_img), cv2.COLOR_RGB2BGR)
        curr_imgcv2 = cv2.resize(curr_imgcv2,(224,224))
        results = classify_image(interpreter, curr_imgcv2)
        save = 0
        for det in results:
            label_id, prob = det
            if (labels[label_id] == 'birds'):

                save = 1
        if (save==1):
            #os.rename(PICS_DIRECTORY+filename, SORTED_PICS+filename)
            pics2move.append(filename)
            save = 0
        else:
            # os.rename(PICS_DIRECTORY+filename, EMPTY_PICS+filename)
            pics4deletion.append(filename)
        counter += 1

    tend_global = datetime.datetime.now()
    print("Finished sorting")
    print("Total pics: "+str(counter))
    print("Total time: "+str(tend_global - tstart_global))
    # move pics
    print("Moving pics")
    for file in pics2move:
        os.rename(PICS_DIRECTORY+file, SORTED_PICS+file)
    # delete pics
    print("Deleting pics")
    for file in pics4deletion:
        os.remove(PICS_DIRECTORY+file)
    # delete lockfile
    os.remove(SCRIPTS_DIR+LOCKFILE)
    exit(0)

if __name__ == '__main__':
  main()
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import cv2

import glob
import os
import re
import datetime
from PIL import Image, ImageDraw
import numpy as np
# import tensorflow

from tflite_runtime.interpreter import Interpreter

def load_labels(path):
  with open(path, 'r') as f:
    return {i: line.strip() for i, line in enumerate(f.readlines())}


def set_input_tensor(interpreter, image):
  tensor_index = interpreter.get_input_details()[0]['index']
  input_tensor = interpreter.tensor(tensor_index)()[0]
  input_tensor[:, :] = image


def classify_image(interpreter, image, top_k=1):
  """Returns a sorted array of classification results."""
  set_input_tensor(interpreter, image)
  interpreter.invoke()
  output_details = interpreter.get_output_details()[0]
  output = np.squeeze(interpreter.get_tensor(output_details['index']))

  # If the model is quantized (uint8 data), then dequantize the results
  if output_details['dtype'] == np.uint8:
    scale, zero_point = output_details['quantization']
    output = scale * (output - zero_point)

  ordered = np.argpartition(-output, top_k)
  return [(i, output[i]) for i in ordered[:top_k]]


def main():
    # labels = load_labels("birds_labels.txt")
    labels = load_labels("inat_bird_labels.txt")

    # interpreter = Interpreter("mobilenet_v2_1.0_224_quant_edgetpu.tflite",experimental_delegates=[Interpreter.load_delegate('libedgetpu.so.1')])
    # interpreter = Interpreter("mobilenet_v2_1.0_224_quant_edgetpu.tflite",experimental_delegates=[tensorflow.lite.experimental.load_delegate('edgetpu.dll')])
    # interpreter = Interpreter("mobilenet_v2_1.0_224.tflite")
    interpreter = Interpreter("mobilenet_v2_1.0_224_inat_bird_quant.tflite")
    interpreter.allocate_tensors()
    _, height, width, _ = interpreter.get_input_details()[0]['shape']
    counter = 0
    os.chdir("D:\\BIRBS\\today\\birds")
    if (not os.path.exists("sorted")):
       os.mkdir("sorted")

    tstart = datetime.datetime.now()
    tstart_global = tstart
    len=glob.glob('*.jpg').__len__()

    for filename in glob.glob('*.jpg'):

        if (counter % 500 == 0 and counter > 0):
            tend = datetime.datetime.now()
            time_diff = (tend - tstart)
            tdelta = time_diff.total_seconds()
            print("Number of pictures processed: " + str(counter))
            print("Progress: " + str(round(counter*100/len,2)) + "%")
            print("Processing time per 500 pics: " + str(tdelta))
            print("Rate: " + str(round(500/tdelta,3))+"pics/s")
            tstart = datetime.datetime.now()

        curr_img = Image.open(filename).convert('RGB')
        curr_imgcv2 = cv2.cvtColor(np.array(curr_img), cv2.COLOR_RGB2BGR)
        curr_imgcv2 = cv2.resize(curr_imgcv2,(224,224))
        results = classify_image(interpreter, curr_imgcv2)
        # print(result)
        save = 0
        # draw = ImageDraw.Draw(curr_img)
        label_id = 964 # id of background
        for det in results:
            label_id, prob = det
            # if (labels[label_id] == 'birds'):
            #     save = 1
            if (prob > 0.1 and label_id != 964):
                save = 1
        if (save==1):
            label = re.search('\(([^)]+)', labels[label_id]).group(1)
            birdname = label.replace(" ","_").replace("'","").replace('"', '')
            os.rename(filename, "sorted\\"+birdname+"_"+filename)
            save = 0
        counter += 1

    tend_global = datetime.datetime.now()
    print("Total pics: "+str(counter))
    print("Total time: "+str(tend_global - tstart_global))

if __name__ == '__main__':
  main()
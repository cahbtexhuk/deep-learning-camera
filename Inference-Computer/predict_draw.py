from darkflow.net.build import TFNet
import cv2

import os
import time
from io import BytesIO
import datetime
import requests
from PIL import Image, ImageDraw
import numpy as np


import glob

options = {"model": "cfg/birb-yolov2-tiny.cfg", "load": "cfg/birb-yolov2-tiny-training_best.weights", "threshold": 0.6}

tfnet = TFNet(options)


counter = 0
os.chdir("F:\\BIRBS\\27.08")
if (not os.path.exists("sorted")):
   os.mkdir("sorted")

tstart = datetime.datetime.now()
tstart_global = tstart
len=glob.glob('*.jpg').__len__()

for filename in glob.glob('*.jpg'):

    if (counter % 500 == 0 and counter > 0):
        tend = datetime.datetime.now()
        time_diff = (tend - tstart)
        tdelta = time_diff.total_seconds()
        print("Number of pictures processed: " + str(counter))
        print("Progress: " + str(round(counter*100/len,2)) + "%")
        print("Processing time per 500 pics: " + str(tdelta))
        print("Rate: " + str(round(500/tdelta,3))+"pics/s")
        tstart = datetime.datetime.now()

    curr_img = Image.open(filename).convert('RGB')
    curr_imgcv2 = cv2.cvtColor(np.array(curr_img), cv2.COLOR_RGB2BGR)

    result = tfnet.return_predict(curr_imgcv2)
    # print(result)
    save = 0
    # draw = ImageDraw.Draw(curr_img)
    for det in result:
        if (det['label'] == 'bird'):
            # draw.rectangle([det['topleft']['x'], det['topleft']['y'],
            #                 det['bottomright']['x'], det['bottomright']['y']],
            #                outline=(255, 0, 0))
            # draw.text([det['topleft']['x'], det['topleft']['y'] - 13], det['label'], fill=(255, 0, 0))
            save = 1
    if (save==1):
        # curr_img.save('birds_labeled/%i.jpg' % counter)
        os.rename(filename, "sorted\\"+filename)
        save = 0
    counter += 1

tend_global = datetime.datetime.now()
print("Total pics: "+str(counter))
print("Total time: "+str(tend_global - tstart_global))
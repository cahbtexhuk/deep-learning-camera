import os
import sys
import hashlib
import glob


if __name__ == '__main__':
    os.chdir("F:\\bird-ai\\classifier-pics")
    unsorted = glob.glob("*.txt")
    filelist = sorted(unsorted, key=len)
    for file in filelist:
        filename, file_extension = os.path.splitext(file)
        if os.stat(file).st_size > 0:
            try:
                print("Trying to move: " + filename)
                os.rename(filename+".txt","birds\\"+filename+".txt")
                os.rename(filename+".jpg","birds\\"+filename+".jpg")

            except Exception as e:
                print("Error displaying file name.")
        else:
            try:
                print("Trying to move: " + filename)
                os.rename(filename + ".txt", "not-birds\\" + filename + ".txt")
                os.rename(filename + ".jpg", "not-birds\\" + filename + ".jpg")

            except Exception as e:
                print("Error displaying file name.")

    print("Finished")
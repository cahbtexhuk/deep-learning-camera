import os
import sys
import hashlib
import glob


if __name__ == '__main__':
    os.chdir("F:\\Google Drive\\yolo-training\\data")
    unsorted = glob.glob("*.jpg")
    filelist = sorted(unsorted, key=len)
    for file in filelist:
        if not file == "main.py":
            filename, file_extension = os.path.splitext(file)
            try:
                print("Trying to move: " + file)
                if os.path.isfile(filename + ".txt"):
                    print("Annotation already exists")
                else:
                    print("creating empty annotation " + file)
                    # os.rename(file, "sorted\\" + file)
                    open(filename+".txt", "a").close()
            except Exception as e:
                print("Error displaying file name.")

    print("Finished")
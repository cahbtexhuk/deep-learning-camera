# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.


def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press Ctrl+F8 to toggle the breakpoint.


import os
import sys
import hashlib
import glob

def hash(file):
    if not os.path.isdir(file):
        f = open(file, 'rb')
        sum = hashlib.sha1(f.read()).hexdigest()
        return sum
    else:
        return "dir"

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    os.chdir("E:\\bird-ai\\birds\\new-batch\\not-birds")

    unsorted = glob.glob("*.jpg")
    filelist = sorted(unsorted, key=len)

    for file in filelist:
        if not file == "main.py":

            filename, file_extension = os.path.splitext(file)
            if not file_extension == ".txt":
                sum = hash(file)
            try:
                print("Trying to rename: " + file)

                if os.path.isfile(sum + file_extension):
                    print("file " + sum + file_extension + " already exists")
                    if not file == (sum + file_extension):
                        print("Removing: " + file + " because it is a duplicate")
                        os.remove(file)

                elif not sum == "dir":
                    os.rename(file, sum + file_extension)
                    print(file + ' --> ' + sum + file_extension)
                    if os.path.isfile(filename + ".txt"):
                        print("renaming txt file as well")
                        os.rename(filename + ".txt", sum + ".txt")
                else:
                    print("Skipping directory " + file)

            except Exception as e:
                print("Error displaying file name.")

        print("")

# See PyCharm help at https://www.jetbrains.com/help/pycharm/

import matplotlib.pyplot as plt
import numpy as np
import os
os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

import tensorflow as tf


assert tf.__version__.startswith('1')

tf.enable_eager_execution()
gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.3)
sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options))

from tensorflow import keras
# PATH = "C:\\Users\\ireha\\Documents\\poor-mans-deep-learning-camera\\mobilenet\\train"
PATH = "E:\\bird-ai\\birds\\new-batch"



IMAGE_SIZE = 224
BATCH_SIZE = 64
def main():
    datagen = tf.keras.preprocessing.image.ImageDataGenerator(
        rescale=1./255,
        validation_split=0.3)

    train_generator = datagen.flow_from_directory(
        PATH,
        target_size=(IMAGE_SIZE, IMAGE_SIZE),
        batch_size=BATCH_SIZE,
        subset='training')

    val_generator = datagen.flow_from_directory(
        PATH,
        target_size=(IMAGE_SIZE, IMAGE_SIZE),
        batch_size=BATCH_SIZE,
        subset='validation')
    image_batch, label_batch = next(val_generator)
    image_batch.shape, label_batch.shape

    print(train_generator.class_indices)

    labels = '\n'.join(sorted(train_generator.class_indices.keys()))

    with open('birds_labels.txt', 'w') as f:
        f.write(labels)

    IMG_SHAPE = (IMAGE_SIZE, IMAGE_SIZE, 3)

    # Create the base model from the pre-trained MobileNet V2
    base_model = tf.keras.applications.MobileNetV2(input_shape=IMG_SHAPE,
                                                   include_top=False,
                                                   weights='imagenet')
    base_model.trainable = False

    model = tf.keras.Sequential([
        base_model,
        tf.keras.layers.Conv2D(filters=32, kernel_size=3, activation='relu'),
        tf.keras.layers.Dropout(0.2),
        tf.keras.layers.GlobalAveragePooling2D(),
        tf.keras.layers.Dense(units=2, activation='softmax')
    ])

    model.compile(optimizer=tf.keras.optimizers.Adam(),
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])

    model.summary()

    print('Number of trainable weights = {}'.format(len(model.trainable_weights)))
    checkpoint_path = "training/mobilenet-{epoch:04d}-{val_loss:.2f}.h5"
    cp_callback = tf.keras.callbacks.ModelCheckpoint(
        filepath=checkpoint_path,
        verbose=1,
        save_best_only=True,
        save_weights_only=False,
        save_freq='epoch')

    history = model.fit_generator(train_generator,
                                  epochs=10,
                                  validation_data=val_generator,
                                  callbacks=[cp_callback])

    acc = history.history['acc']
    val_acc = history.history['val_acc']

    loss = history.history['loss']
    val_loss = history.history['val_loss']

    plt.figure(figsize=(8, 8))
    plt.subplot(2, 1, 1)
    plt.plot(acc, label='Training Accuracy')
    plt.plot(val_acc, label='Validation Accuracy')
    plt.legend(loc='lower right')
    plt.ylabel('Accuracy')
    plt.ylim([min(plt.ylim()), 1])
    plt.title('Training and Validation Accuracy')

    plt.subplot(2, 1, 2)
    plt.plot(loss, label='Training Loss')
    plt.plot(val_loss, label='Validation Loss')
    plt.legend(loc='upper right')
    plt.ylabel('Cross Entropy')
    plt.ylim([0, 1.0])
    plt.title('Training and Validation Loss')
    plt.xlabel('epoch')
    plt.show()

    print("Number of layers in the base model: ", len(base_model.layers))

    base_model.trainable = True
    fine_tune_at = 100

    # Freeze all the layers before the `fine_tune_at` layer
    for layer in base_model.layers[:fine_tune_at]:
        layer.trainable = False

    model.compile(optimizer=tf.keras.optimizers.Adam(1e-5),
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])

    model.summary()

    print('Number of trainable weights = {}'.format(len(model.trainable_weights)))

    history_fine = model.fit_generator(train_generator,
                                       epochs=5,
                                       validation_data=val_generator)

    acc = history_fine.history['acc']
    val_acc = history_fine.history['val_acc']

    loss = history_fine.history['loss']
    val_loss = history_fine.history['val_loss']

    plt.figure(figsize=(8, 8))
    plt.subplot(2, 1, 1)
    plt.plot(acc, label='Training Accuracy')
    plt.plot(val_acc, label='Validation Accuracy')
    plt.legend(loc='lower right')
    plt.ylabel('Accuracy')
    plt.ylim([min(plt.ylim()), 1])
    plt.title('Training and Validation Accuracy')

    plt.subplot(2, 1, 2)
    plt.plot(loss, label='Training Loss')
    plt.plot(val_loss, label='Validation Loss')
    plt.legend(loc='upper right')
    plt.ylabel('Cross Entropy')
    plt.ylim([0, 1.0])
    plt.title('Training and Validation Loss')
    plt.xlabel('epoch')
    plt.show()

    saved_keras_model = 'model.h5'
    model.save(saved_keras_model)

    converter = tf.lite.TFLiteConverter.from_keras_model_file(saved_keras_model)
    converter.optimizations = [tf.lite.Optimize.DEFAULT]
    tflite_model = converter.convert()

    with open('mobilenet_v2_1.0_224.tflite', 'wb') as f:
        f.write(tflite_model)

    # A generator that provides a representative dataset
    def representative_data_gen():
        dataset_list = tf.data.Dataset.list_files(PATH + '/*')
        for i in range(100):
            image = next(iter(dataset_list))
            image = tf.io.read_file(image)
            image = tf.io.decode_jpeg(image, channels=3)
            image = tf.image.resize(image, [IMAGE_SIZE, IMAGE_SIZE])
            image = tf.cast(image / 255., tf.float32)
            image = tf.expand_dims(image, 0)
            yield [image]

    saved_keras_model = 'model.h5'
    model.save(saved_keras_model)

    converter = tf.lite.TFLiteConverter.from_keras_model_file(saved_keras_model)
    converter.optimizations = [tf.lite.Optimize.DEFAULT]
    # This ensures that if any ops can't be quantized, the converter throws an error
    converter.target_spec.supported_ops = [tf.lite.OpsSet.TFLITE_BUILTINS_INT8]
    # These set the input and output tensors to uint8
    converter.inference_input_type = tf.uint8
    converter.inference_output_type = tf.uint8
    # And this sets the representative dataset so we can quantize the activations
    converter.representative_dataset = representative_data_gen
    tflite_model = converter.convert()

    with open('mobilenet_v2_1.0_224_quant.tflite', 'wb') as f:
        f.write(tflite_model)

if __name__ == '__main__':
  main()
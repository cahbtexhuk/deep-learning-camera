#!/usr/bin/python3
from luma.core.interface.serial import i2c
from luma.core.render import canvas
from luma.oled.device import ssd1306
from time import sleep
import subprocess
import psutil
import glob

serial = i2c(port=1, address=0x3C)
device = ssd1306(serial, rotate=2) # rotate=2 is 180 degrees
with canvas(device) as draw:
    draw.text((10, 20), "Init", fill="white")
sleep(1)
# First define some constants to allow easy resizing of shapes.
width = 128
height = 64
padding = -2
top = padding
bottom = height-padding
# Move left to right keeping track of the current x position for drawing shapes.
x = 0
while True:
    with canvas(device) as draw:

        # Draw a black filled box to clear the image.
        draw.rectangle((0,0,width,height), outline=0, fill=0)

        # Shell scripts for system monitoring from here : https://unix.stackexchange.com/questions/119126/command-to-display-memory-usage-disk-usage-and-cpu-load
        cmd = "hostname -I | cut -d\' \' -f1"
        IP = subprocess.check_output(cmd, shell = True )
        cmd = "top -bn1 | grep load | awk '{printf \"CPU Load: %.2f\", $(NF-2)}'"
        CPU = subprocess.check_output(cmd, shell = True )
        cmd = "free -m | awk 'NR==2{printf \"Mem: %s/%sMB %.2f%%\", $3,$2,$3*100/$2 }'"
        MemUsage = subprocess.check_output(cmd, shell = True )
        cmd = "df -h | awk '$NF==\"/\"{printf \"Disk: %d/%dGB %s\", $3,$2,$5}'"
        Disk = subprocess.check_output(cmd, shell = True )
        # cmd = "ls /home/pi/birds/ | wc -l"
        # Files = subprocess.check_output(cmd, shell = True )
        Files = glob.glob('birds/*.jpg').__len__()
        if "motion" in (p.name() for p in psutil.process_iter()):
            Motion = "running"
        else:
            Motion = "stopped"

        # Write two lines of text.
        draw.text((x, top),       "IP: " + str(IP,'utf-8'), fill=255)
        draw.text((x, top+8),     str(CPU,'utf-8'), fill=255)
        draw.text((x, top+16),    str(MemUsage,'utf-8'), fill=255)
        draw.text((x, top+24),    str(Disk,'utf-8'),fill=255)
        draw.text((x, top+32),    "Motion is " + Motion,fill=255)
        draw.text((x, top+40),    "Pics: " + str(Files,'utf-8'), fill=255)

    sleep(5)

